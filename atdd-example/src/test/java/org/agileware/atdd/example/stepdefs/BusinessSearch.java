package org.agileware.atdd.example.stepdefs;

import static org.junit.Assert.assertEquals;
import static org.openqa.selenium.By.id;

import java.util.List;

import org.agileware.test.web.SharedWebDriver;
import org.seleniumhq.selenium.fluent.FluentWebDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BusinessSearch {
	protected SharedWebDriver browser;

	private static String baseURL() {
		return System.getProperty("webapp.base.url");
	}

	@Before
	public void before() {
		browser = SharedWebDriver.open();
	}

	@After
	public void after() {
		browser.close();
		// This does nothing and could be completely removed.
		// It is here for compatibility reasons so that if we decide to switch
		// to a non shared web driver implementation we do not need to change
		// the step definitions.
	}

	@Given("^the user is logged in$")
	public void the_user_is_logged_in() throws InterruptedException {
		browser.get(baseURL() + "/index.jsp");
		FluentWebDriver fluent = new FluentWebDriver(browser);
		assertEquals(100, fluent.links().size());
	}

	@When("^performs a business search for \"([^\"]*)\"$")
	public void performs_a_business_search_for(String search) {
		browser.get(baseURL() + "/business/search");
		FluentWebDriver fluent = new FluentWebDriver(browser);
		fluent.input(id("input")).sendKeys(search);
		fluent.input(id("submit")).click();
	}

	@When("^performs an advanced business search for \"([^\"]*)\"$")
	public void performs_an_advanced_business_search_for(String search) {
		browser.get(baseURL() + "/business/search");
		FluentWebDriver fluent = new FluentWebDriver(browser);
		fluent.input(id("input")).sendKeys(search);
		fluent.input(id("submit")).click();
	}

	@Then("^the results shown on screen should be$")
	public void the_results_shown_on_screen_should_be(List<Entity> results) {
		// FluentWebElements rows = browser.trs(cssSelector("#results tr"));
		// assertEquals(2, rows.size());
	}

	public static class Entity {
		public String name;
		private String url;

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}
}
