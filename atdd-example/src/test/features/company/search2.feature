@wip
Feature: Referrals

  Write a description of your feature <here>..

@wip9Regression
Scenario Outline: Show referral screen when insurance cover for contents or stock at your premises is selected 

Given I am on the CGU Insurance Portal for Tradies HomePage
When I enter the Occupation type that I need cover for "<Occupation>"
And I enter the State where my business operates in "<State>"
And I click the Get a Quote button
And the CGU Insurance Portal Displays the "Get a Quote" page
And I enter the number of full time employees in my business "<Full time employees>"
And I enter whether or not I require cover for my contents or stock "<Contents or Stock Cover>"
And I enter the primary reason for looking for insurance "<level of insurance cover>"
And I accept the default Insurance Start Date
And I enter how much Public Liability cover I need "<Public Liability cover>"
And I enter how much Tools cover I need "<Tools cover>"
And I enter how much Onsite Tools cover I need "<Tools Onsite cover>"
And I enter whether or not I need portable electronic equipment cover "<Portable Electronic Equipment>"
And I disclose whether or not I use labour hire in my business "<Business Labour Hire>"
And I disclose whether or not I perform any of the listed activities in my business "<Business activities performed>"
And I click the Review your quote button
Then the soft referral screen is displayed when insurance cover for contents or stock is selected "<Contents or Stock Cover>"

Examples:
        | Occupation							|	State   |Contents or Stock Cover|	Full time employees |	Insurance Start Date	| level of insurance cover  													| Public Liability cover	|   Tools cover 	|  Tools Onsite cover 	| Portable Electronic Equipment	| Business Labour Hire 	| Business activities performed | Items apply to you 	| Agree Product Disclosure Statement 	| calculated quote amount per month	|	calculated quote amount per year		|
  		|Sign Erection & Installation			|	SA		|	Yes					|	8					|	26/12/2013				|	I want comprehensive protection so my business and tools are well covered	|	$20 million				|	$10,000			|	$2,500				| Yes:$3,000					| Yes:90,000			|	No							|	No					|	Yes									|	261.79							|	3,141.44								|
  		|Security Doors & Shutter Installation	|	SA		|	Yes					|	8					|	26/12/2013				|	I want comprehensive protection so my business and tools are well covered	|	$20 million				|	$10,000			|	$2,500				| Yes:$3,000					| Yes:90,000			|	No							|	No					|	Yes									|	261.79							|	3,141.44								|
  	
